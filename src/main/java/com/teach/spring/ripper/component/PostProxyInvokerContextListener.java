package com.teach.spring.ripper.component;

import com.teach.spring.ripper.annotation.PostProxy;
import java.lang.reflect.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public class PostProxyInvokerContextListener implements ApplicationListener<ContextRefreshedEvent> {

  @Autowired
  private ConfigurableListableBeanFactory configurableListableBeanFactory;

  @Override
  public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
    ApplicationContext context = contextRefreshedEvent.getApplicationContext();
    String[] names = context.getBeanDefinitionNames();
    for (String name : names) {
      if (name != null) {
        BeanDefinition beanDefinition = configurableListableBeanFactory.getBeanDefinition(name);
        String originalClassName = beanDefinition.getBeanClassName();
        try {
          Class<?> originalClass = Class.forName(originalClassName);
          Method[] methods = originalClass.getMethods();
          for (Method method : methods) {
            if (method.isAnnotationPresent(PostProxy.class)) {
              Object bean = context.getBean(name);
              Method currentMethod = bean.getClass()
                  .getMethod(method.getName(), method.getParameterTypes());
              currentMethod.invoke(bean);
            }
          }

        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }
}
