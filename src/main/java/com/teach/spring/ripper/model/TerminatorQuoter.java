package com.teach.spring.ripper.model;

import com.teach.spring.ripper.annotation.DeprecatedClass;
import com.teach.spring.ripper.annotation.InjectRandomInt;
import com.teach.spring.ripper.annotation.PostProxy;
import com.teach.spring.ripper.screensaver.Profiling;
import javax.annotation.PostConstruct;

@Profiling
@DeprecatedClass(newImpl = SecondClass.class)
public class TerminatorQuoter implements Quoter {

  @InjectRandomInt(min = 2, max = 7)
  private int repeat;

  private String message;

  @PostConstruct
  public void init() {
    System.out.println("TerminatorQuoter init method");
    System.out.println("repeat = " + repeat);
  }

  public TerminatorQuoter() {
    System.out.println("TerminatorQuoter constructor");
    System.out.println("repeat = " + repeat);
  }

  public void setRepeat(int repeat) {
    this.repeat = repeat;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  @PostProxy
  public void sayQuote() {
    System.out.println("TerminatorQuoter sayQuote");
    for (int i = 0; i < repeat; i++) {
      System.out.println(message);
    }
  }
}
