package com.teach.spring.ripper.model;

public class SecondClass extends TerminatorQuoter {

  @Override
  public void sayQuote() {
    System.out.println("Say quote method from SecondClass");
  }
}
