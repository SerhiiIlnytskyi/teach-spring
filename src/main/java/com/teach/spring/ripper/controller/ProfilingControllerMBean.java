package com.teach.spring.ripper.controller;

public interface ProfilingControllerMBean {

  void setEnabled(boolean enabled);

}
